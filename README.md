## Quick start

This project requires [Node.js][nodejs] 6.2+, [NPM][npm], [Ruby][ruby] and [Bundler][bundler]. So just make sure you have those installed. Then just type following commands:

```
git clone https://admoko@bitbucket.org/admoko/helloapi.git
cd api-blueprint-boilerplate
npm install
bundle install
npm start
```

You now have a local preview server available at [http://localhost:8080](http://localhost:8080) that is validating and rendering your blueprint using [Apiary CLI][apiaryio-cli].

## Testing against specification

To test the blueprint run `npm test` command. This will validate the schema using Drafter.js library.

## Export HTML files

To compile the blueprint run `npm run build`. Files will be written to the `build` directory.

## Publishing to Apiary

To publish your blueprint to [Apiary.io][apiaryio] you have to set two environment variables:

```
export APIARY_API_KEY="<token>"
export APIARY_API_NAME="<apiary project name>"
```

After that, you can run `npm run publish` command to upload your blueprint to Apiary project. You can also provide those two values before the command

```
APIARY_API_KEY="<token>" APIARY_API_NAME="<apiary project name>" npm run publish
```
